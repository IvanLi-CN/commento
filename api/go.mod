module gitlab.com/commento/commento/api

go 1.12

require (
	github.com/adtac/go-akismet v0.0.0-20181220032308-0ca9e1023047
	github.com/disintegration/imaging v1.6.2
	github.com/gomodule/oauth1 v0.0.0-20181215000758-9a59ed3b0a84
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/mux v1.6.2
	github.com/lib/pq v0.0.0-20180523175426-90697d60dd84
	github.com/lunny/html2md v0.0.0-20180317074532-13aaeeae9fb2
	github.com/markbates/goth v1.73.1 // indirect
	github.com/microcosm-cc/bluemonday v1.0.0
	github.com/op/go-logging v0.0.0-20160211212156-b2cb9fa56473
	github.com/russross/blackfriday v1.5.1
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
)
