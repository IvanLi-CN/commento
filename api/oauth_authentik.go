package main

import (
	"os"

	"github.com/markbates/goth/providers/openidConnect"
	"golang.org/x/oauth2"
)

var authentikProvider *openidConnect.Provider
var authentikConfig *oauth2.Config

func authentikOauthConfigure() error {
	authentikConfig = nil
	authentikProvider = nil
	if os.Getenv("AUTHENTIK_KEY") == "" && os.Getenv("AUTHENTIK_SECRET") == "" && os.Getenv("AUTHENTIK_ENDPOINT") == "" {
		return nil
	}

	if os.Getenv("AUTHENTIK_KEY") == "" {
		logger.Errorf("COMMENTO_AUTHENTIK_KEY not configured, but COMMENTO_AUTHENTIK_SECRET is set")
		return errorOauthMisconfigured
	}

	if os.Getenv("AUTHENTIK_SECRET") == "" {
		logger.Errorf("COMMENTO_AUTHENTIK_SECRET not configured, but COMMENTO_AUTHENTIK_KEY is set")
		return errorOauthMisconfigured
	}
	if os.Getenv("AUTHENTIK_ENDPOINT") == "" {
		logger.Errorf("COMMENTO_AUTHENTIK_ENDPOINT not configured, but COMMENTO_AUTHENTIK_KEY is set")
		return errorOauthMisconfigured
	}

	logger.Infof("loading authentik Open ID Connect config")

	provider, err := openidConnect.New(os.Getenv("AUTHENTIK_KEY"), os.Getenv("AUTHENTIK_SECRET"), os.Getenv("ORIGIN")+"/api/oauth/authentik/callback", os.Getenv("AUTHENTIK_ENDPOINT"), []string{"email", "profile", "openid"}...)
	authentikProvider = provider

	if err != nil {
		logger.Errorf("loading authentik Open ID Connect config failed. %v", err)
		return errorOauthDiscoveryFailed
	}
	authentikConfig = &oauth2.Config{
		RedirectURL:  os.Getenv("ORIGIN") + "/api/oauth/authentik/callback",
		ClientID:     authentikProvider.ClientKey,
		ClientSecret: authentikProvider.Secret,
		Scopes:       []string{"email", "profile", "openid"},
		Endpoint: oauth2.Endpoint{
			AuthURL:   authentikProvider.OpenIDConfig.AuthEndpoint,
			TokenURL:  authentikProvider.OpenIDConfig.TokenEndpoint,
			AuthStyle: oauth2.AuthStyle(1),
		},
	}

	authentikConfigured = true

	return nil
}
